from aiohttp import web
from controller import dog, toy

routes = web.RouteTableDef()

@routes.get('/')
async def index(request):
    return web.Response(text='Start working')

async def init_app()->web.Application:
    app = web.Application()
    app.add_routes(routes)
    app.add_routes(dog.routes)
    app.add_routes(toy.routes)
    return app

web.run_app(init_app())