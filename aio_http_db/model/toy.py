from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from .base import Base
from model.dog import Dog

class Toy(Base):
    __tablename__ = 'toy'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', String)
    dog_id = Column(Integer, ForeignKey(Dog.id), nullable=False)

    def __init__(self, name, dog_id):
        self.name = name
        self.dog_id = dog_id