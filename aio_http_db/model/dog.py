from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from .base import Base

class Dog(Base):
    __tablename__ = 'dog'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', String)
    age = Column('age', Integer)

    toys = relationship('Toy')

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __repr__(self):
        return f"Dog name is {self.name} and have {self.age} age"