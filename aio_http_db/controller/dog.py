from aiohttp import web
from model.dog import Dog
from model.base import Session, Base, engine

routes = web.RouteTableDef()
session = Session()
Base.metadata.create_all(engine)

@routes.post('/add_dog')
async def add_dog(request):
    data_dog = await request.json()
    name = data_dog['name']
    age = data_dog['age']
    dog_obj = Dog(name, age)
    session.add(dog_obj)
    session.commit()
    dog_dict = {'name' : dog_obj.name}
    return web.json_response(dog_dict)

@routes.get('/alldogs')
async def alldogs(request):
    result = [i for i in session.query(Dog).all()]
    my_list = []
    for i in result:
        dog_dict = {"id" : i.id,
                    "name" : i.name,
                    "age" : i.age}
        my_list.append(dog_dict)
    return web.json_response(my_list)

@routes.get('/dog/{id}')
async def get_dog(request):
    id = request.match_info.get("id")
    dog = session.query(Dog).filter_by(id=id).first()

    dog_dict = {
        "name":dog.name,
        "age":dog.age
    }
    session.close()
    return  web.json_response(dog_dict, status=200)

@routes.put("/dog/{id}")
async def update(request):
    id = request.match_info.get("id")
    dog = session.query(Dog).filter_by(id=id).first()
    data = await request.json()

    dog.name = data['name']
    dog.age = data['age']

    session.add(dog)
    session.commit()

    dog_dict = {
        "name" : dog.name,
        "age" : dog.age
    }

    session.close()
    return web.json_response(dog_dict, status=200)

@routes.delete("/dog/{id}")
async def delete(request):
    id = request.match_info.get("id")
    dog = session.query(Dog).filter_by(id=id).first()
    session.delete(dog)
    session.commit()
    session.close()
    return web.json_response(text="Dog deleted", status=200)