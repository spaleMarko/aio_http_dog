from aiohttp import web
from model.dog import Dog
from model.toy import Toy
from model.base import Session, Base, engine

routes = web.RouteTableDef()
session = Session()
Base.metadata.create_all(engine)

@routes.post("/addtoy")
async def create_toy(request):
    data = await request.json()
    name = data['name']
    dog_id = data['dog_id']
    toy_object = Toy(name, dog_id)
    session.add(toy_object)
    session.commit()
    toy_dict = {'name':toy_object.name,
                'dog_id':toy_object.dog_id}
    return web.json_response(toy_dict)